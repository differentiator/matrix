#include <iostream>
#include "Matrix.h"
#include <complex>

int main() {
//    Matrix<double> l(1, 3, 2, 2);
    auto l = Matrix<double>::eye(1);
    std::cout << "Hello, Matrix!" << std::endl;
    l.print();
//    l.print();
    ULONG iters = 15;
    auto x = l.inverse_schulz_iteration(iters);
    x.print();
    (x*l).print();
    std::cout << l.det() << std::endl;
    return 0;
}