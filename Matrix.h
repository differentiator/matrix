//
// Created by teslenko on 2019-07-16.
//
//
#ifndef LINEARALGEBRA_MATRIX_H
#define LINEARALGEBRA_MATRIX_H

#include <iostream>
#include <string>
#include <algorithm>
#include <cmath>

using ULONG = unsigned long;

template<class T>
class Matrix {
private:
    ULONG _rows, _cols;
    T **data;
public:
    Matrix(ULONG rows = 1, ULONG cols = 1, T fill = 0);
    Matrix(T start, T step, ULONG rows, ULONG cols);
    Matrix(const Matrix &rhs);
    ~Matrix();

    void AllocateSpace();
    void DeallocateSpace();
    void print() const;

    Matrix add(const Matrix &) const;
    Matrix sub(const Matrix &) const;
    Matrix prod(const Matrix &) const;


    Matrix &operator=(const Matrix &rhs);
    Matrix &operator+=(const Matrix &);
    Matrix &operator-=(const Matrix &);
    Matrix &operator*=(const Matrix &);
    Matrix &operator*=(T &);
    Matrix &operator/=(T &);

//        Matrix  operator^(ULONG);
//
//        // Optimized algorithms
//
//        //Helpers
    static Matrix eye(ULONG n);
    Matrix transpose();
    T det();

//        Matrix inverse();
    Matrix inverse_schulz_iteration(ULONG &);
    void fill(const T &value);
//        void pow(const ULONG &value);
    void SwapRows(ULONG, ULONG);

    ULONG getCols() const;
    ULONG getRows() const;
    void put(ULONG &, ULONG &, T &);
    T get(ULONG &, ULONG &);

};

template<class T>
Matrix<T> operator+(const Matrix<T> &lhs, const Matrix<T> &rhs);

template<class T>
Matrix<T> operator-(const Matrix<T> &lhs, const Matrix<T> &rhs);

template<class T>
Matrix<T> operator*(const Matrix<T> &lhs, const Matrix<T> &rhs);

template<class T>
Matrix<T> operator*(const T &val, const Matrix<T> &rhs);

template<class T>
Matrix<T> operator/(const Matrix<T> &rhs, const T &val);


template<class T>
Matrix<T> operator/(const Matrix<T> &rhs, const T &val) {
    Matrix<T> copy(rhs);
    for (ULONG i = 0; i < copy.getRows(); i++) {
        for (ULONG j = 0; j < copy.getCols(); j++) {
            T div = copy.get(i, j) / val;
            copy.put(i, j, div);
        }
    }
    return copy;
}


template<class T>
Matrix<T> operator*(const T &val, const Matrix<T> &rhs) {
    Matrix<T> copy(rhs);
    for (ULONG i = 0; i < copy.getRows(); i++) {
        for (ULONG j = 0; j < copy.getCols(); j++) {
            T prod = copy.get(i, j) * val;
            copy.put(i, j, prod);
        }
    }
    return copy;
}


template<class T>
Matrix<T> operator+(const Matrix<T> &lhs, const Matrix<T> &rhs) {
    return lhs.add(rhs);
}

template<class T>
Matrix<T> operator-(const Matrix<T> &lhs, const Matrix<T> &rhs) {
    return lhs.sub(rhs);
}

template<class T>
Matrix<T> operator*(const Matrix<T> &lhs, const Matrix<T> &rhs) {
    return lhs.prod(rhs);
}

template<class T>
void Matrix<T>::AllocateSpace() {
    data = new T *[_rows];
    for (ULONG i = 0; i < _rows; i++) {
        data[i] = new T[_cols];
    }
}

template<class T>
void Matrix<T>::DeallocateSpace() {
    for (ULONG i = 0; i < _rows; i++) {
        delete[] data[i];
    }
    delete[] data;
}

template<class T>
Matrix<T>::Matrix(ULONG rows, ULONG cols, T fill): _rows(rows), _cols(cols) {
    data = new T *[rows];
    for (ULONG i = 0; i < rows; i++) {
        data[i] = new T[cols];
        for (ULONG j = 0; j < cols; j++) {
            data[i][j] = fill;
        }
    }
}

template<class T>
Matrix<T>::Matrix(T start, T step, ULONG rows, ULONG cols): _rows(rows), _cols(cols) {
    T acc = start;
    data = new T *[rows];
    for (ULONG i = 0; i < rows; i++) {
        data[i] = new T[cols];
        for (ULONG j = 0; j < cols; j++) {
            data[i][j] = acc;
            acc += step;
        }
    }
}

template<class T>
Matrix<T>::Matrix(const Matrix &rhs) : _rows(rhs._rows), _cols(rhs._cols) {
    data = new T *[_rows];
    for (ULONG i = 0; i < _rows; i++) {
        data[i] = new T[_cols];
        for (ULONG j = 0; j < _cols; j++) {
            data[i][j] = rhs.data[i][j];
        }
    }
}

template<class T>
Matrix<T>::~Matrix() {
    DeallocateSpace();
}

template<class T>
void Matrix<T>::print() const {
    for (ULONG i = 0; i < _rows; i++) {
        for (ULONG j = 0; j < _cols; j++) {
            std::cout << data[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}


template<class T>
Matrix<T> &Matrix<T>::operator=(const Matrix &rhs) {
    if (this == &rhs) {
        return *this;
    }
    if (_rows != rhs._rows || _cols != rhs._cols) {
        DeallocateSpace();
        _rows = rhs._rows;
        _cols = rhs._cols;
        AllocateSpace();
    }
    for (ULONG i = 0; i < _rows; i++) {
        for (ULONG j = 0; j < _cols; j++) {
            data[i][j] = rhs.data[i][j];
        }
    }
    return *this;
}

template<class T>
Matrix<T> &Matrix<T>::operator+=(const Matrix &rhs) {
    //TODO check rhs sizes for matching
    for (ULONG i = 0; i < _rows; i++) {
        for (ULONG j = 0; j < _cols; j++) {
            data[i][j] += rhs.data[i][j];
        }
    }
    return *this;
}

template<class T>
Matrix<T> &Matrix<T>::operator-=(const Matrix &rhs) {
    //TODO check rhs sizes for matching
    for (ULONG i = 0; i < _rows; i++) {
        for (ULONG j = 0; j < _cols; j++) {
            data[i][j] -= rhs.data[i][j];
        }
    }
    return *this;
}

template<class T>
Matrix<T> &Matrix<T>::operator*=(const Matrix &rhs) {
    //TODO check this and rhs sizes for matching
    Matrix<T> product(_rows, rhs._cols);
    for (ULONG i = 0; i < _rows; i++) {
        for (ULONG j = 0; j < rhs._cols; j++) {
            for (ULONG k = 0; k < _cols; ++k) {
                product.data[i][j] += (data[i][k] * rhs.data[k][j]);
            }
        }
    }
    return (*this = product);
}

template<class T>
Matrix<T> &Matrix<T>::operator*=(T &val) {
    for (ULONG i = 0; i < _rows; i++) {
        for (ULONG j = 0; j < _cols; j++) {
            data[i][j] *= val;
        }
    }
    return *this;
}

template<class T>
Matrix<T> &Matrix<T>::operator/=(T &val) {
    for (ULONG i = 0; i < _rows; i++) {
        for (ULONG j = 0; j < _cols; j++) {
            data[i][j] /= val;
        }
    }
    return *this;
}

//
//template<class T>
//Matrix Matrix<T>::operator^(ULONG) {
//    return Matrix(0, 0, false);
//}
//
template<class T>
Matrix<T> Matrix<T>::transpose() {
    Matrix<T> transposed(_cols, _rows);
    for (ULONG i = 0; i < _rows; i++) {
        for (ULONG j = 0; j < _cols; j++) {
            transposed.data[j][i] = data[i][j];
        }
    }
    return transposed;
}

//
//template<class T>
//Matrix Matrix<T>::inverse() {
//    return Matrix(0, 0, false);
//}
//
template<class T>
void Matrix<T>::fill(const T &value) {
    for (ULONG i = 0; i < _rows; i++) {
        for (ULONG j = 0; j < _cols; j++) {
            data[i][j] = value;
        }
    }
}

//
//template<class T>
//void Matrix<T>::pow(const ULONG &value) {
//
//}
//
template<class T>
void Matrix<T>::SwapRows(ULONG i, ULONG j) {
    //TODO check i,j for out of bounds
    std::swap(data[i], data[j]);
}

template<class T>
Matrix<T> Matrix<T>::eye(ULONG n) {
    Matrix<T> identity(n, n, 0);
    for (ULONG i = 0; i < n; i++) {
        identity.data[i][i] = T(1);
    }
    return identity;
}

template<class T>
Matrix<T> Matrix<T>::add(const Matrix<T> &rhs) const {
    //TODO check this and rhs sizes for matching
    Matrix<T> result(_rows, _cols);
    for (ULONG i = 0; i < _rows; i++) {
        for (ULONG j = 0; j < _cols; j++) {
            result.data[i][j] = data[i][j] + rhs.data[i][j];
        }
    }
    return result;
}

template<class T>
Matrix<T> Matrix<T>::sub(const Matrix<T> &rhs) const {
    //TODO check this and rhs sizes for matching
    Matrix<T> result(_rows, _cols);
    for (ULONG i = 0; i < _rows; i++) {
        for (ULONG j = 0; j < _cols; j++) {
            result.data[i][j] = data[i][j] - rhs.data[i][j];
        }
    }
    return result;
}

template<class T>
Matrix<T> Matrix<T>::prod(const Matrix<T> &rhs) const {
    //TODO check this and rhs sizes for matching
    Matrix<T> product(_rows, rhs._cols);
    for (ULONG i = 0; i < _rows; i++) {
        for (ULONG j = 0; j < rhs._cols; j++) {
            for (ULONG k = 0; k < _cols; ++k) {
                product.data[i][j] += (data[i][k] * rhs.data[k][j]);
            }
        }
    }
    return product;
}

template<class T>
Matrix<T> Matrix<T>::inverse_schulz_iteration(ULONG &max_iter) {
    //TODO check if invertible(if det==0)
    Matrix<T> eye2 = T(2)*Matrix<T>::eye(_rows);
    T alpha = det();
    alpha = T(1) / (alpha * alpha);
    Matrix<T> inv(*this);
    inv *= alpha;
    for (ULONG i = 0; i < max_iter; i++) {
        inv *= (eye2 - (this->prod(inv)));
    }
    return inv;
}

template<class T>
ULONG Matrix<T>::getCols() const {
    return _cols;
}

template<class T>
ULONG Matrix<T>::getRows() const {
    return _rows;
}

template<class T>
void Matrix<T>::put(ULONG &i, ULONG &j, T &val) {
    data[i][j] = val;
}

template<class T>
T Matrix<T>::get(ULONG &i, ULONG &j) {
    return data[i][j];
}

template<class T>
T Matrix<T>::det() {
    //TODO check if matrix is square
    Matrix<T> rhs(*this);
    T det = 1;
    T minus_one = -1;
    T zero = 0;
    for (ULONG i = 0; i < _rows; i++) {
        T pivot_elem = rhs.data[i][i];
        ULONG pivot_index = i;
        for (ULONG row = i + 1; row < _rows; ++row) {
            if (std::abs(rhs.data[row][i]) > std::abs(pivot_elem)) {
                pivot_elem = rhs.data[row][i];
                pivot_index = row;
            }
        }
        if (pivot_elem == zero) {
            return zero;
        }
        if (pivot_index != i) {
            rhs.SwapRows(i, pivot_index);
            det *= minus_one;
        }
        det *= pivot_elem;
        for (ULONG row = i + 1; row < _rows; ++row) {
            for (ULONG col = i + 1; col < _rows; ++col) {
                rhs.data[row][col] -= rhs.data[row][i] * rhs.data[i][col] / pivot_elem;
            }
        }
    }
    return det;
}

#endif //LINEARALGEBRA_MATRIX_H


